from tools.creator import FakeData
from tools.db import DataBase


class Application(DataBase):
    def __init__(self, data: FakeData) -> None:
        self.data = data

    async def init(self):
        await self.connect()
        profiles = await self.data.make_profile()
        await self.insert()
        return profiles
