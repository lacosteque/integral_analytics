from dataclasses import dataclass
from datetime import time
from typing import Iterable, Type


@dataclass(slots=True, frozen=True)
class Time:
    start: time
    finish: time


@dataclass(slots=True, frozen=True)
class WorkTime(Time):
    pass


@dataclass(slots=True, frozen=True)
class Slot(Time):
    pass


@dataclass(slots=True, frozen=True)
class Employee:
    id: int
    name: str
    surname: str
    middle_name: str
    position: str
    work_time: WorkTime
    all_slots: Iterable[Slot]
    busy_slots: Iterable[Slot] | None = None
