import asyncio
from collections import Counter
from itertools import chain

from faker import Faker

from core.app import Application
from tools.calendar import Calendar
from tools.creator import FakeData

INTERVAL = 120
PROFILES = 25

fake = Faker()
app = Application(data=FakeData(fake, interval=INTERVAL, qty=PROFILES))


async def main():
    profiles = await app.init()

    print('\n\033[31m[Profiles]\033[0m \n')
    for profile in profiles:
        print(
            f'  \033[33m[{profile.id}]\033[0m - {profile.name} {profile.surname} - [{profile.position}]'
        )

    while True:
        print('\n\033[31m[Menu]\033[0m \n')
        menu = ('Show work time', 'Show free time slot')
        for k, i in enumerate(menu):
            print(f'  \033[33m[{k+1}]\033[0m - {i}')
        menu = int(input())
        ids = list(
            map(
                int,
                input('  \033[34m[?]\033[0m - Enter profile id: ').split(),
            )
        )

        match menu:

            case 1:
                for id_ in ids:
                    calendar = Calendar(employee=profiles[id_])
                    wt = await calendar.get_office_hours()
                    print(
                        f"  \033[35m[!]\033[0m - {profiles[id_].name}'s work time: {wt.start} - {wt.finish}"
                    )
            case 2:
                merge = []
                for id_ in ids:
                    calendar = Calendar(profiles[id_])
                    free_slots = await calendar.get_free_slots()
                    merge += free_slots

                if merge:

                    if len(ids) == 1:
                        print(
                            f"  \033[35m[!]\033[0m - {profiles[ids[0]].name}'s free slots:"
                        )
                        return await Calendar.choose_time_slot(merge)

                    else:

                        employees = ', '.join(
                            profile.name
                            for profile in profiles
                            if profile.id in ids
                        )

                        print(
                            f'  \033[35m[!]\033[0m - Free slots for the group [{employees}]'
                        )
                        free_slots = [
                            k
                            for k, v in Counter(list(chain(merge))).items()
                            if v == len(ids)
                        ]
                    return await Calendar.choose_time_slot(free_slots)


if __name__ == '__main__':
    asyncio.run(main())
