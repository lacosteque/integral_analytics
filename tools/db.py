import asyncio
import random


class DataBase:

    NAME = 'env.name'
    PASSWORD = 'env.password'
    USER = 'env.user'
    HOST = 'env.localhost'
    PORT = 'envport'

    async def sleep(self) -> None:
        timeout = random.choice((1, 2, 3, 4, 5))
        await asyncio.sleep(timeout)

    async def connect(self) -> None:
        print(
            f'  \033[32m[+]\033[0m - Database connected | {self.USER}:{self.PASSWORD}@{self.HOST}:{self.PORT}/{self.NAME}'
        )
        await self.sleep()

    async def fetch(self) -> None:
        print('  \033[32m[+]\033[0m - fetch data')
        await self.sleep()

    async def insert(self) -> None:
        print('  \033[32m[+]\033[0m - insert data')
        await self.sleep()
