import datetime
import random

from faker import Faker

from personal import Employee, Slot, WorkTime
from tools.converter import Converter


class FakeData(Converter):
    def __init__(self, fake: Faker, interval: int = 60, qty: int = 1) -> None:
        self.fake = fake
        self.interval = interval * 60
        self.time = datetime.time
        self.qty = qty

    async def __make_work_time(self) -> WorkTime:
        start_hours = random.choice((7, 8, 9, 10))
        start_minutes = 0
        finish_hours = random.choice((17, 18, 19, 20, 21, 22, 23))
        finish_minutes = random.choice((0, 15, 30, 45))

        return WorkTime(
            start=self.time(start_hours, start_minutes),
            finish=self.time(finish_hours, finish_minutes),
        )

    async def __make_slots(
        self, work_time: WorkTime
    ) -> tuple[list[Slot], list[Slot | None]]:

        convert_start_time = (
            await self.minute_to_seconds(work_time.start.minute),
            await self.hour_to_seconds(work_time.start.hour),
        )
        convert_finish_time = (
            await self.minute_to_seconds(work_time.finish.minute),
            await self.hour_to_seconds(work_time.finish.hour),
        )

        s1 = sum(map(lambda result: result.value, convert_start_time))

        s2 = sum(map(lambda result: result.value, convert_finish_time))

        work_hours = s2 - s1
        max_slots = work_hours // self.interval
        qty = random.choice(tuple(i for i in range(max_slots)))
        all_slots = []
        timestamp = await self.make_timestamp(work_time)
        for i in range(max_slots):

            dt = datetime.datetime.fromtimestamp(
                timestamp.value + (self.interval * i + self.interval)
            )
            dt2 = datetime.datetime.fromtimestamp(
                timestamp.value + (self.interval * i)
            )

            slot = Slot(dt2.time(), dt.time())

            all_slots.append(slot)

        busy_slots = random.sample(all_slots, qty)

        return all_slots, busy_slots

    async def make_profile(self) -> list[Employee]:
        profiles = []
        for i in range(self.qty):
            work_time = await self.__make_work_time()
            all_slots, busy_slots = await self.__make_slots(work_time)

            template = {
                'id': i,
                'name': self.fake.first_name(),
                'surname': self.fake.last_name(),
                'middle_name': None,
                'position': self.fake.job(),
                'work_time': work_time,
                'all_slots': all_slots,
                'busy_slots': busy_slots,
            }
            i = Employee(**template)
            profiles.append(i)
        print('  \033[32m[+]\033[0m - profiles have been created')
        return profiles
