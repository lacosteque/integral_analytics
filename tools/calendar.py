from collections import Counter
from typing import Iterable

from personal import Employee, Slot, WorkTime
from tools.db import DataBase


class Calendar(DataBase):
    def __init__(self, employee: Employee) -> None:
        self.employee = employee

    async def get_office_hours(self) -> WorkTime:
        await self.fetch()
        return self.employee.work_time

    async def get_free_slots(self) -> Iterable[Slot]:
        await self.fetch()
        all_slots = self.employee.all_slots
        busy_slots = self.employee.busy_slots
        return list((Counter(all_slots) - Counter(busy_slots)).elements())

    @staticmethod
    async def choose_time_slot(free_slots: list[Slot]) -> bool:

        if not free_slots:
            print('No slots available, Bye, Bye !!!')
            return True
        for i, slot in enumerate(free_slots):
            print(
                f'  \033[33m[{i}]\033[0m - {str(slot.start)} - {str(slot.finish)}'
            )

        ids = sorted(
            list(
                map(
                    int,
                    input('  \033[34m[?]\033[0m - Enter slot id: ').split(),
                )
            ),
            reverse=True,
        )

        await DataBase().insert()
        print('  \033[35m[!]\033[0m - booked:')
        for slot in map(free_slots.pop, ids):
            print(f'        [{str(slot.start)} - {str(slot.finish)}]')
        return False
