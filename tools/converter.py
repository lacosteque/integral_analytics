import datetime
from dataclasses import dataclass

from personal import WorkTime


@dataclass(slots=True, frozen=True)
class Seconds:
    value: int


class Converter:
    NOW = datetime.date.today()

    async def minute_to_seconds(self, minutes: int) -> Seconds:
        return Seconds(minutes * 60)

    async def hour_to_seconds(self, hours: int) -> Seconds:
        return Seconds(hours * 3600)

    async def make_timestamp(self, work_time: WorkTime) -> Seconds:
        dt = datetime.datetime(
            self.NOW.year,
            self.NOW.month,
            self.NOW.day,
            work_time.start.hour,
            work_time.start.minute,
        )

        return Seconds(int(datetime.datetime.timestamp(dt)))
