import asyncio
import datetime
import random
from dataclasses import dataclass
from pprint import pprint

from faker import Faker

from personal import Employee, Slot, WorkTime


@dataclass(slots=True, frozen=True)
class Seconds:
    value: int


class Converter:
    NOW = datetime.date.today()

    async def minute_to_seconds(self, minutes: int) -> Seconds:
        return Seconds(minutes * 60)

    async def hour_to_seconds(self, hours: int) -> Seconds:
        return Seconds(hours * 3600)

    async def make_timestamp(self, work_time: WorkTime) -> int:
        dt = datetime.datetime(
            self.NOW.year,
            self.NOW.month,
            self.NOW.day,
            work_time.start.hour,
            work_time.start.minute,
        )

        return int(datetime.datetime.timestamp(dt))


class Calendar:
    def __init__(self, employee: Employee) -> None:
        self.employee = employee

    async def get_office_hours(self):
        return self.employee.work_time

    async def get_free_time_slots(self):
        pass

    async def choose_time_slot(self):
        pass


class FakeData(Converter):
    def __init__(self, fake: Faker, interval: int, qty: int = 1) -> None:
        self.fake = fake
        self.interval = interval * 60
        self.time = datetime.time
        self.qty = qty

    async def __make_slot(self):
        pass

    async def __make_work_time(self) -> WorkTime:
        start_hours = random.choice((7, 8, 9, 10))
        start_minutes = random.choice((0, 15, 30, 45))
        finish_hours = random.choice((17, 18, 19, 20))
        finish_minutes = random.choice((0, 15, 30, 45))

        return WorkTime(
            start=self.time(start_hours, start_minutes),
            finish=self.time(finish_hours, finish_minutes),
        )

    async def __make_busy_slots(
        self, work_time: WorkTime
    ) -> list[Slot | None]:

        convert_start_time = (
            await self.minute_to_seconds(work_time.start.minute),
            await self.hour_to_seconds(work_time.start.hour),
        )
        convert_finish_time = (
            await self.minute_to_seconds(work_time.finish.minute),
            await self.hour_to_seconds(work_time.finish.hour),
        )

        s1 = sum(map(lambda result: result.value, convert_start_time))

        s2 = sum(map(lambda result: result.value, convert_finish_time))

        work_hours = s2 - s1
        max_slots = work_hours // self.interval
        qty = random.choice(tuple(i for i in range(max_slots)))
        busy_slots = []
        timestamp = await self.make_timestamp(work_time)
        for i in range(max_slots):

            dt = datetime.datetime.fromtimestamp(
                timestamp + (self.interval * i + self.interval)
            )
            dt2 = datetime.datetime.fromtimestamp(
                timestamp + (self.interval * i)
            )

            slot = Slot(dt2.time(), dt.time())

            busy_slots.append(slot)

        return random.sample(busy_slots, qty)

    async def make_profile(self) -> list[Employee]:
        profiles = []
        for i in range(self.qty):
            work_time = await self.__make_work_time()
            busy_slots = await self.__make_busy_slots(work_time)

            template = {
                'id': i,
                'name': self.fake.first_name(),
                'surname': self.fake.last_name(),
                'middle_name': None,
                'position': self.fake.job(),
                'work_time': work_time,
                'busy_slots': busy_slots,
            }
            profiles.append(Employee(**template))

        return profiles


data = FakeData(Faker(), 120, 3)

asyncio.run(data.make_profile())
